using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour
{
    private float waitTime = 30.0f;
    private float timer;
    [SerializeField] Image image;

    private void OnEnable()
    {
        timer = waitTime;
    }

    void Update()
    {
        timer -= Time.deltaTime;
        image.fillAmount = timer / waitTime;

        if (timer < 0.0f)
        {
            GameManager.Instance.ClearGrids();
            GameManager.Instance.ChangePlayerRound();
        }
    }
}
