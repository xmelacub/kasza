using Kasza.Singletons;
using Unity.Netcode;
using UnityEngine;

public class Player : Singleton<Player>
{
    [SerializeField] CreateGrid grid;
    public int playerId;
    void Awake()
    {
        if (NetworkManager.Singleton.IsHost ) {
            Debug.Log(playerId + "host ");
            playerId = 0;
            grid.Init();
        }
        else playerId = 1;
    }


}
