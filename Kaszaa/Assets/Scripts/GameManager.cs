using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Kasza.Singletons;
public class GameManager : NetworkSingleton<GameManager>
{
    
    [SerializeField] TextMeshProUGUI currentLevel;
    [SerializeField] GameObject pawnInfo;
    [SerializeField] public List<GameObject> decks;
    [SerializeField] GameObject clock;

    public List<GameObject> activeGrids = new();
    public GameObject[,] gridTab;
    public Grid[] lastGridTab;
    public GameObject gridParent;
    public GameObject pawnChoosed;
    public GameObject specialPawnChoosed;

    public int verticalGridAmount=5;
    public int horizontalGridAmount=7;
    public bool[,] gridUseTab;
    public int playerRound = 0;

    // TODO:
    // healerka 
    // ogarnac tego decka ( losowanie w multiplayer??? )
    // UI
    // camera
    // przejzec skrypty

    public void CreateGrid()
    {
        gridTab = new GameObject[ horizontalGridAmount, verticalGridAmount];
        gridUseTab= new bool[horizontalGridAmount, verticalGridAmount];
        lastGridTab = new Grid[verticalGridAmount*2];
    }
    public void ChangeTabValues(int xPos, int yPos,int x,int y)
    {
        gridUseTab[xPos, yPos] = false;
        gridUseTab[x, y] = true;
        pawnChoosed.transform.parent = gridTab[x, y].transform;
        pawnChoosed.transform.position = gridTab[x, y].transform.position;
        ChangePlayerRound();
    }
    public void ChangePlayerRound()
    {
        if (playerRound == 0) playerRound = 1;
        else playerRound = 0;
        currentLevel.text = "Current Player: " + playerRound.ToString();
        CheckLastGridTab();
        clock.SetActive(false);
        clock.SetActive(true);
    }
    public void ChangePawnInfo(Pawn pawn)
    {
        pawnInfo.SetActive(true);
        pawnInfo.GetComponent<PawnInfo>().ChangeInfo(pawn);
    }
    public void ChangePawnInfo()
    {
        pawnInfo.SetActive(false);
    }
    public void ClearGrids()
    {
        for (int i = activeGrids.Count - 1; i >= 0; i--)
        {
            activeGrids[i].GetComponent<Grid>().SetGridOrginalMaterial();
            activeGrids.RemoveAt(i);
        }
        pawnChoosed = null;
    }
    private void CheckLastGridTab()
    {
        for (int i=0; i < lastGridTab.Length; i++)
        {
            lastGridTab[i].CheckLastGrid();
        }
    }


}
