using TMPro;
using UnityEngine;

public class PawnInfo : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI Hp;
    [SerializeField] TextMeshProUGUI Dmg;
    [SerializeField] TextMeshProUGUI SpecialName;
    public void ChangeInfo(Pawn pawn)
    {
        Hp.text = pawn.GetHp().ToString();
        Dmg.text = pawn.GetDmg().ToString();
        SpecialName.text = pawn.GetSpecialName();
    }
}
