using Unity.Netcode;
using UnityEngine;

public class CreateGrid : MonoBehaviour
{
    [SerializeField] GameObject grid;
    [SerializeField] float xPos= -1.6f;
    [SerializeField] float yPos=0;
    [SerializeField] float zPos=2.4f;
    private float xSpace;
    private float zSpace;
    private float verticalDistance = 4;
    private float horizontalDistance = 5.6f;

   public void Init()
    {

        GameManager.Instance.gridParent = this.gameObject;
        xSpace = verticalDistance / GameManager.Instance.verticalGridAmount;
        zSpace = horizontalDistance / GameManager.Instance.horizontalGridAmount;
        Create();
    }
    private void Create()
    {
        int specialGrid = 1;
        float x = xPos;
        float z = zPos;
        int lastGridIndex = 0;
        GameManager.Instance.CreateGrid();

        for(int i= 0; i < GameManager.Instance.horizontalGridAmount; i++)
        {
            for(int j=0; j < GameManager.Instance.verticalGridAmount; j++)
            {
                GameObject myObject = Instantiate(grid,new Vector3(0,0,0), Quaternion.identity);

                myObject.transform.position = new Vector3(x, yPos, z);
                myObject.GetComponent<NetworkObject>().Spawn(true);

                //myObject.transform.parent = this.gameObject.transform;
                GameManager.Instance.gridTab[i, j] = myObject;
                myObject.transform.GetChild(0).GetComponent<Grid>().SetPosition(i, j);
                if (specialGrid == 1 || specialGrid == GameManager.Instance.horizontalGridAmount)
                {
                    GameManager.Instance.lastGridTab[lastGridIndex] = myObject.transform.GetChild(0).GetComponent<Grid>();
                    lastGridIndex++;
                }
                x = x + xSpace;
            }
            specialGrid++;
            z = z - zSpace;
            x = xPos;
        }
    }
}
