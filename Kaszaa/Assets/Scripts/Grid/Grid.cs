using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class Grid : NetworkBehaviour
{
	[SerializeField] Material highlight;
	[SerializeField] Material orginal;
	[SerializeField] Material selected;
	[SerializeField] Material attack;
	[SerializeField] Material special;
	private Material lastMaterial;
	private int xPos;
	private int yPos;
	private List<Vector2> pawnMovesTab = new();
	private List<Vector2> pawnAttackTab = new();
	private bool isOnGrid = false;
	private int horizontalGridAmount;
	private int verticalGridAmount;
	public int firstPlayerOnGrid;


	private void Start()
	{
		lastMaterial = orginal;
		horizontalGridAmount = GameManager.Instance.horizontalGridAmount;
		verticalGridAmount = GameManager.Instance.verticalGridAmount;
	}
	public void CheckLastGrid()
	{
		if (Player.Instance.playerId != GameManager.Instance.playerRound) return;
		if (GameManager.Instance.gridUseTab[xPos,yPos]== false)
		{
			GameManager.Instance.decks[firstPlayerOnGrid].GetComponent<Deck>().InitCardOnScene(this.gameObject.transform.parent);
		}
		else if(GameManager.Instance.gridTab[xPos,yPos].transform.GetChild(2).GetComponent<Pawn>().GetPlayerId() != firstPlayerOnGrid)
		{
			Debug.Log("finish");
		}
	}

	private void OnMouseOver()
	{
		if (Player.Instance.playerId != GameManager.Instance.playerRound) return;
		if (isOnGrid == false)
		{
			gameObject.GetComponent<Renderer>().material = highlight;
			isOnGrid = true;
			if (GameManager.Instance.pawnChoosed == null)
			{
				GameManager.Instance.activeGrids = new();
				SelectGrids();
			}
		}
	}
	private void OnMouseExit()
	{
		if (Player.Instance.playerId != GameManager.Instance.playerRound) return;
		if (GameManager.Instance.pawnChoosed==null)
		{
			DisselectGrids();
			gameObject.GetComponent<Renderer>().material = orginal;
		}
		else
		{
			gameObject.GetComponent<Renderer>().material = lastMaterial;
		}
		isOnGrid = false;
		GameManager.Instance.ChangePawnInfo();

	}
	private void OnMouseDown()
	{
		if (Player.Instance.playerId != GameManager.Instance.playerRound) return;
		if (GameManager.Instance.pawnChoosed == null)
		{
			if (GameManager.Instance.gridTab[xPos, yPos].transform.childCount == 3 && GameManager.Instance.playerRound == GameManager.Instance.gridTab[xPos, yPos].transform.GetChild(2).GetComponent<Pawn>().GetPlayerId())
			{ 
				GameManager.Instance.pawnChoosed = GameManager.Instance.gridTab[xPos, yPos].transform.GetChild(2).gameObject;
				gameObject.GetComponent<Renderer>().material = highlight;
				lastMaterial = highlight;
			}
		}
		else
		{
			if (GameManager.Instance.playerRound == GameManager.Instance.pawnChoosed.GetComponent<Pawn>().GetPlayerId())
			{
				if (GameManager.Instance.pawnChoosed.GetComponent<Pawn>().GetIsSpecial() == false || GameManager.Instance.pawnChoosed.GetComponent<Pawn>().GetSpecialMove() == 0)
				{
					SetGridMaterial(GameManager.Instance.pawnChoosed.GetComponent<Pawn>().FindPawnGrid(), orginal, false);
					GameManager.Instance.pawnChoosed.GetComponent<Pawn>().Move(xPos, yPos);
				}
				else
				{
					GameManager.Instance.pawnChoosed.GetComponent<Pawn>().Move(xPos, yPos);
				}
			}
		}
	}

	void Update()
	{
		if (Input.GetMouseButtonDown(1))
		{
			GameManager.Instance.pawnChoosed = null;
			DisselectGrids();
		}
	}

	public void SetGridOrginalMaterial()
	{
		GetComponent<Renderer>().material = orginal;
		lastMaterial = orginal;
	}
	private void SetGridMaterial(GameObject grid,Material material, bool addToList)
	{
		if (addToList == true)
		{
			GameManager.Instance.activeGrids.Add(grid);
		}
		grid.GetComponent<Renderer>().material = material;
		grid.GetComponent<Grid>().lastMaterial = material;
	}
	public void SetPosition(int x,int y)
	{
		xPos = x;
		yPos = y;
	}
	public Vector2 GetPosition()
	{
		return new Vector2(xPos, yPos);
	}
	private void SelectGrids()
	{
		
		if (GameManager.Instance.gridUseTab[xPos, yPos] == true )
		{
			Pawn tempPawn = GameManager.Instance.gridTab[xPos, yPos].transform.GetChild(2).GetComponent<Pawn>();

			GameManager.Instance.ChangePawnInfo(tempPawn);

			if (GameManager.Instance.playerRound == tempPawn.GetPlayerId()) 
			{
				pawnAttackTab = tempPawn.GetAttackTab();
				pawnMovesTab = tempPawn.GetMovesTab();
				MoveSelect();
				AttackSelect();
			}
		}
	}
	private void MoveSelect()
	{
		for (int i = 0; i < pawnMovesTab.Count; i++)
		{
			if (xPos + pawnMovesTab[i].x < horizontalGridAmount && xPos + pawnMovesTab[i].x >= 0 && yPos + pawnMovesTab[i].y < verticalGridAmount && yPos + pawnMovesTab[i].y >= 0)
			{
				if (GameManager.Instance.gridUseTab[(int)(xPos + pawnMovesTab[i].x), (int)(yPos + pawnMovesTab[i].y)] == false)
				{
					SetGridMaterial(GameManager.Instance.gridTab[(int)(xPos + pawnMovesTab[i].x), (int)(yPos + pawnMovesTab[i].y)].transform.GetChild(0).gameObject, selected,true);
				}
				else
				{
					if (GameManager.Instance.gridTab[xPos, yPos].transform.GetChild(2).GetComponent<Pawn>().GetIsSpecial() == true && GameManager.Instance.gridTab[(int)(xPos + pawnMovesTab[i].x), (int)(yPos + pawnMovesTab[i].y)].transform.GetChild(2).GetComponent<Pawn>().GetPlayerId() == GameManager.Instance.playerRound)
					{
						SetGridMaterial(GameManager.Instance.gridTab[(int)(xPos + pawnMovesTab[i].x), (int)(yPos + pawnMovesTab[i].y)].transform.GetChild(0).gameObject, special,true);
					}
				}
			}
		}
	}
	private void AttackSelect()
	{
		for (int i = 0; i < pawnAttackTab.Count; i++)
		{
			if (xPos + pawnAttackTab[i].x < horizontalGridAmount && xPos + pawnAttackTab[i].x >= 0 && yPos + pawnAttackTab[i].y < verticalGridAmount && yPos + pawnAttackTab[i].y >= 0 && GameManager.Instance.gridUseTab[(int)(xPos + pawnAttackTab[i].x), (int)(yPos + pawnAttackTab[i].y)] == true && GameManager.Instance.gridTab[(int)(xPos + pawnAttackTab[i].x), (int)(yPos + pawnAttackTab[i].y)].transform.GetChild(2).GetComponent<Pawn>().GetPlayerId() != GameManager.Instance.playerRound)
			{ 
				SetGridMaterial(GameManager.Instance.gridTab[(int)(xPos + pawnAttackTab[i].x), (int)(yPos + pawnAttackTab[i].y)].transform.GetChild(0).gameObject,attack,true);
			}
		}
	}
	private void DisselectGrids()
	{
		for (int i = 0; i < pawnMovesTab.Count; i++)
		{
			if (xPos + pawnMovesTab[i].x < horizontalGridAmount && xPos + pawnMovesTab[i].x >= 0 && yPos + pawnMovesTab[i].y < verticalGridAmount && yPos + pawnMovesTab[i].y >= 0)
			{ 
				SetGridMaterial(GameManager.Instance.gridTab[(int)(xPos + pawnMovesTab[i].x), (int)(yPos + pawnMovesTab[i].y)].transform.GetChild(0).gameObject,orginal,false);
			}
		}
	}
	
	


}
