using System.Collections.Generic;
using TMPro;
using Unity.Netcode;
using UnityEngine;

public class Deck : MonoBehaviour
{
    List<int> deck = new List<int>();
    [SerializeField] GameObject cardPrefab;
    [SerializeField] int playerId;

    void Start()
    {
        Debug.Log(NetworkManager.Singleton.IsHost);
        if (NetworkManager.Singleton.IsHost)
        {
            InitDeck();
           SortDeck();
            OpenFirstCards();
        }
    }
    private void InitDeck()
    {
        for (int i = 2; i < 15; i++)
        {
            deck.Add(i);
            deck.Add(i);
        }
    }
    private void SortDeck()
    {
        int cardsLeft = 26;
        List<int> deckTemp = new List<int>();
        int i;
        while (cardsLeft > 0)
        {
            i = Random.Range(0, cardsLeft);
            deckTemp.Add(deck[i]);
            deck.RemoveAt(i);
            cardsLeft--;
        }
        deck = deckTemp;
    }
    private void OpenFirstCards()
    {
        Transform myParent;
        if (playerId == 0)
        {
            for (int j = 0; j < GameManager.Instance.verticalGridAmount ; j++)
            {
                for (int i = 0; i < 2; i++)
                {
                    myParent = GameManager.Instance.gridTab[i, j].gameObject.transform;
                    InitCardOnScene(myParent);
                }
            }
        }
        else
        {
            for (int j = 0; j < GameManager.Instance.verticalGridAmount; j++)
            {
                for (int i = 5; i < 7; i++)
                {
                    myParent = GameManager.Instance.gridTab[i,j].gameObject.transform;
                    InitCardOnScene(myParent);
                }
            }
           
        }
    }
    public void InitCardOnScene(Transform myParent)
    {
        if (deck.Count>0)
        {
            GameObject myObject = Instantiate(cardPrefab);

            myObject.transform.position = myParent.position;
            myObject.GetComponent<NetworkObject>().Spawn();

            myObject.transform.parent = myParent;
            DesignateUseOfPawn(deck[0], myObject);
            myObject.GetComponent<Pawn>().Init(deck[0], myParent.GetChild(0).GetComponent<Grid>().GetPosition(), playerId);
            myObject.transform.GetChild(0).GetComponent<TextMeshPro>().text = deck[0].ToString();
            myParent.GetChild(0).GetComponent<Grid>().firstPlayerOnGrid = playerId;
            deck.RemoveAt(0);
        }

    }

    private void DesignateUseOfPawn(int number, GameObject myObject)
    {
        if (number < 11)
        {
            myObject.AddComponent<Basic>();
        }
        else if (number == 11)
        {
            myObject.AddComponent<Jumper>();
        }
        else if (number == 12)
        {
            myObject.AddComponent<Healer>();

        }
        else if (number == 13)
        {
            myObject.AddComponent<Swiper>();

        }
        else if (number == 14)
        {
            myObject.AddComponent<As>();

        }
    }

}

