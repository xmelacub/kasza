using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public abstract class Pawn : NetworkBehaviour
{
	protected List<Vector2> movesTab = new List<Vector2>();
	protected List<Vector2> attackTab = new List<Vector2>();

	private int playerId;
	protected int number;
	protected int hp;
	protected int damage;
	protected int xPos;
	protected int yPos;
	protected bool isFullMoveRange;
	protected bool isSpecial;
	protected string specialName;
	public int specialMove=0;
	
	public void Init(int _number, Vector2 position,int _playerId)
	{
		number = _number;
		hp = _number;
		xPos = (int)position.x;
		yPos = (int)position.y;
		playerId = _playerId;
		SetMoveRange();
		TakePlace();
		SetDamage();
		MakeMovesTab();
		MakeAttackTab();
		SetSpecialName();
		IsSpecial();
	}
	protected virtual void SetMoveRange()
    {
		isFullMoveRange = false;
    }
	protected virtual void SetDamage()
    {
		damage = number;
    }
	protected virtual void SetSpecialName()
	{
		specialName = "none";
	}
	public void Move(int x, int y)
    {
		if (specialMove == 0)
		{
			if (GameManager.Instance.gridUseTab[x, y] == false)
			{
				ChangePosition(x, y);
				GameManager.Instance.ClearGrids();
			}
			else
			{
				if (GameManager.Instance.gridTab[x, y].transform.GetChild(2).GetComponent<Pawn>().GetPlayerId() != playerId)
				{
					Attack(x, y);
					GameManager.Instance.ClearGrids();

				}
				else if(GameManager.Instance.pawnChoosed != GameManager.Instance.gridTab[x, y].transform.GetChild(2).gameObject)
				{
					Special(x, y);
				}
			}
		}
        else if (GameManager.Instance.gridTab[x, y].transform.childCount == 3 && GameManager.Instance.gridTab[x, y].transform.GetChild(2).GetComponent<Pawn>().GetPlayerId() == playerId && CheckSpecialPawn(x, y))
		{
				Special(x, y);
		}
        else
        {
			GameManager.Instance.pawnChoosed.GetComponent<Pawn>().specialMove = 0;
			GameManager.Instance.specialPawnChoosed = null;
			GameManager.Instance.ClearGrids();
		}
	}
	private bool CheckSpecialPawn(int x, int y)
	{
		for (int i = GameManager.Instance.activeGrids.Count - 1; i >= 0; i--)
		{
			if (GameManager.Instance.activeGrids[i] == GameManager.Instance.gridTab[x, y].transform.GetChild(0).gameObject && GameManager.Instance.gridTab[x, y].transform.childCount == 3 && GameManager.Instance.gridTab[x, y].transform.GetChild(2).gameObject != GameManager.Instance.specialPawnChoosed)
			{
				return true;
			}
		}
		return false;
	}
	public int GetPlayerId()
    {
		return playerId;
    }
	private void TakePlace()
	{
		if(GameManager.Instance.gridUseTab[xPos, yPos]==false)
			GameManager.Instance.gridUseTab[xPos, yPos] = true;
	}
	private void ChangePosition(int x, int y)
    {
		if (Mathf.Abs(xPos - x) <= 1 && Mathf.Abs(yPos - y) <= 1)
		{
			if (xPos != x && yPos == y || xPos == x && yPos != y)
			{
				GameManager.Instance.ChangeTabValues(xPos, yPos, x, y);
				xPos = x;
				yPos = y;
			}
			else if (isFullMoveRange && xPos != x && yPos != y)
			{
				GameManager.Instance.ChangeTabValues(xPos, yPos, x, y);
				xPos = x;
				yPos = y;
			}
		}
	}
	protected void Attack(int x, int y)
    {
		if (Mathf.Abs(xPos - x) <= 1 && Mathf.Abs(yPos - y) <= 1)
		{
			if (xPos != x && yPos == y || xPos == x && yPos != y)
			{
				GameManager.Instance.gridTab[x, y].transform.GetChild(2).GetComponent<Pawn>().TakeDamage(number);
				GameManager.Instance.ChangePlayerRound();

			}
			else if (isFullMoveRange && xPos != x && yPos != y)
			{
				GameManager.Instance.gridTab[x, y].transform.GetChild(2).GetComponent<Pawn>().TakeDamage(number);
				GameManager.Instance.ChangePlayerRound();

			}
		}
	}
	protected virtual void Special(int x, int y)
    {

    }
	public void TakeDamage(int amount)
    {
		if (hp > amount)
		{
			hp = hp - amount;
		}
        else
        {
			GameManager.Instance.gridUseTab[xPos, yPos] = false;
			Destroy(gameObject);
        }
    }
	protected virtual void IsSpecial()
    {
		isSpecial = false;
    }
	protected virtual void MakeMovesTab()
    {
		movesTab.Add(new Vector2(0, -1));
		movesTab.Add(new Vector2(-1, 0));
		movesTab.Add(new Vector2(1, 0));
		movesTab.Add(new Vector2(0, 1));
    }
	protected virtual void MakeAttackTab()
	{
		attackTab.Add(new Vector2(0, -1));
		attackTab.Add(new Vector2(-1, 0));
		attackTab.Add(new Vector2(1, 0));
		attackTab.Add(new Vector2(0, 1));
	}
	public List<Vector2> GetMovesTab()
    {
		return movesTab;
    }
	public List<Vector2> GetAttackTab()
	{
		return attackTab;
	}
	public bool GetIsSpecial()
    {
		return isSpecial;
    }
	public GameObject FindPawnGrid()
    {
		return GameManager.Instance.gridTab[xPos, yPos].transform.GetChild(0).gameObject;
    }
	public int GetHp() { return hp; }
	public int GetDmg() { return damage; }
	public string GetSpecialName() { return specialName; }
	public int GetSpecialMove() { return specialMove; }

	public Vector2 GetPosition() { return new Vector2(xPos, yPos); }

	public void SetPosition(int x, int y)
    {
		xPos = x;
		yPos = y;
    }
}
