using UnityEngine;

public class Healer : Pawn
{
    protected override void SetDamage()
    {
        damage = 10;
    }
    protected override void SetSpecialName()
    {
        specialName = "heal";
    }

}
