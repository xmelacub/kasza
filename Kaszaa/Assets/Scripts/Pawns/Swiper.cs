using UnityEngine;

public class Swiper : Pawn
{
	protected override void SetDamage()
	{
		damage = 10;
	}
	protected override void IsSpecial()
	{
		isSpecial = true;
	}
	protected override void SetSpecialName()
	{
		specialName = "swipe";
	}
	protected override void Special(int x, int y)
	{
		if (specialMove == 0)
		{
			GameManager.Instance.specialPawnChoosed = GameManager.Instance.gridTab[x, y].transform.GetChild(2).gameObject;
			specialMove++;
		}
		else
		{
			Swipe(x,y);
			specialMove = 0;
			GameManager.Instance.specialPawnChoosed = null;
			GameManager.Instance.ClearGrids();
		}
	}
	private void Swipe(int x,int y) // zrobic jakas ladniejsza funkcje
    {
		Vector2 tempPos = GameManager.Instance.specialPawnChoosed.GetComponent<Pawn>().GetPosition();
		GameObject tempObj1 = GameManager.Instance.gridTab[x, y].transform.GetChild(2).gameObject;
		GameObject tempObj2 = GameManager.Instance.gridTab[(int)tempPos.x, (int)tempPos.y].transform.GetChild(2).gameObject;

		tempObj2.transform.parent = GameManager.Instance.gridTab[x, y].transform;
		tempObj2.transform.localPosition = new Vector3(0, 0, 0);
		tempObj1.transform.parent = GameManager.Instance.gridTab[(int)tempPos.x, (int)tempPos.y].transform;
		tempObj1.transform.localPosition = new Vector3(0, 0, 0);

		GameManager.Instance.gridTab[x, y].transform.GetChild(2).GetComponent<Pawn>().SetPosition(x, y);
		GameManager.Instance.gridTab[(int)tempPos.x, (int)tempPos.y].transform.GetChild(2).GetComponent<Pawn>().SetPosition((int)tempPos.x, (int)tempPos.y);

	}


}
