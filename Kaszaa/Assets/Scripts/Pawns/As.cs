using UnityEngine;

public class As : Pawn
{
    protected override void SetMoveRange()
    {
        isFullMoveRange = true;
    }
    protected override void SetDamage()
    {
        damage = 10;
    }
    protected override void MakeMovesTab()
    {
        movesTab.Add(new Vector2(0, -1));
        movesTab.Add(new Vector2(-1, -1));
        movesTab.Add(new Vector2(-1, 1));
        movesTab.Add(new Vector2(1, -1));
        movesTab.Add(new Vector2(1, 1));
        movesTab.Add(new Vector2(-1, 0));
        movesTab.Add(new Vector2(1, 0));
        movesTab.Add(new Vector2(0, 1));
    }
    protected override void MakeAttackTab()
    {
        attackTab.Add(new Vector2(0, -1));
        attackTab.Add(new Vector2(-1, -1));
        attackTab.Add(new Vector2(-1, 1));
        attackTab.Add(new Vector2(1, -1));
        attackTab.Add(new Vector2(1, 1));
        attackTab.Add(new Vector2(-1, 0));
        attackTab.Add(new Vector2(1, 0));
        attackTab.Add(new Vector2(0, 1));
    }

}
