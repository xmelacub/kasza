using UnityEngine;


public class PlayerCamera : MonoBehaviour
{
    [SerializeField] int playerId;
    [SerializeField] GameObject canvas;

    void Start()
    {
         if(Player.Instance.playerId != playerId)
        {
            gameObject.SetActive(false);
        }
        else
        {
            canvas.GetComponent<Canvas>().worldCamera = GetComponent<Camera>();
        }
    }

}
