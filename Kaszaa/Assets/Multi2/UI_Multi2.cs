using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI_Multi2 : MonoBehaviour
{
    [SerializeField] Button host;
    [SerializeField] Button client;
    [SerializeField] TextMeshProUGUI playersInGameText;

    void Start()
    {
        host.onClick.AddListener(() =>
        {
           
            if (NetworkManager.Singleton.StartHost())
            {
                SceneManager.LoadSceneAsync("Game");
            }
            else
            {
                Debug.Log("cant host");
            }

        });
        client.onClick.AddListener(() =>
        {
            
           
                if (NetworkManager.Singleton.StartClient())
                {
                SceneManager.LoadSceneAsync("Game1");
            }
            else
                {
                    Debug.Log("cant client");

                }

        });


    }

    void Update()
    {
        playersInGameText.text = $"Players in game: {PlayerManager.Instance.PlayersInGame}";
        if (PlayerManager.Instance.PlayersInGame==1)
        {
           

        }

    }
}
